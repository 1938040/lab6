package lab6;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
//Jamin Huang 1938040
public class RpsChoice implements EventHandler<ActionEvent>{
	private TextField msg;
	private TextField wins;
	private TextField losses;
	private TextField ties;
	private String choice;
	private RpsGame game;
	
	@Override
	public void handle(ActionEvent e) {

		msg.setText(game.playRound(choice));
		wins.setText("Wins: "+game.getwins());
		losses.setText("Losses: "+game.getlosts());
		ties.setText("Ties: "+game.getties());		
	}
	
	public RpsChoice(TextField msg,TextField wins,TextField losses, TextField ties, String choice, RpsGame game) {
		this.msg=msg;
		this.wins=wins;
		this.losses=losses;
		this.ties=ties;
		this.choice=choice;
		this.game=game;
	}
}
