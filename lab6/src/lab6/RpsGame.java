package lab6;
import java.util.Random;
//Jamin Huang 1938040
public class RpsGame {
	private int numGames;
	private int wins;
	private int losts;
	private int ties;
	private Random ran=new Random();
	
	public RpsGame() {
		numGames=0;
		wins=0;
		losts=0;
		ties=0;
	}
	public String playRound(String choice) {
		int o=ran.nextInt(3);
		String comp="error";
		switch(o) {
			case 0:
				comp="Rock";
				break;
			case 1:
				comp="Paper";
				break;
			case 2:
				comp="Scissors";
				break;
		}
		if(choice.equals(comp)) {
			ties++;
			return "Computer chooses "+comp+".A draw!";
		}
		else if(choice.equals("Rock")&&comp.equals("Scissors")||choice.equals("Scissors")&&comp.equals("Paper")||choice.equals("Paper")&&comp.equals("Rock")) {
			wins++;
			return "Computer plays "+comp+".Player wins!";
		}
		else {
			losts++;
			return "Computer plays "+comp+".Computer wins!";
		}
	}
	public int getnumGames() {
		return numGames;
	}
	public int getwins() {
		return wins;
	}
	public int getlosts() {
		return losts;
	}
	public int getties() {
		return ties;
	}
}
