package lab6;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
//Jamin Huang 1938040
public class RpsApplication extends Application {	
	private RpsGame game;	
	public void start(Stage stage) {
		Group root = new Group(); 
		game=new RpsGame();
		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);

		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 
		
		Button Rock=new Button("Rock");
		Button Paper=new Button("Paper");
		Button Scissors=new Button("Scissors");
		TextField msg=new TextField("Welcome");
		TextField losses=new TextField("Lost: 0");
		TextField wins=new TextField("Wins: 0");
		TextField ties=new TextField("Ties: 0");
		
		msg.setPrefWidth(200);
		
		HBox buttons=new HBox();
		HBox textfields=new HBox();
		VBox overall=new VBox();
		
		buttons.getChildren().addAll(Rock,Paper,Scissors);
		textfields.getChildren().addAll(msg,losses,wins,ties);
		overall.getChildren().addAll(buttons,textfields);
		root.getChildren().addAll(overall);
		
    	RpsChoice rock=new RpsChoice(msg,losses,wins,ties,"Rock",game);
    	RpsChoice paper=new RpsChoice(msg,losses,wins,ties,"Paper",game);
    	RpsChoice scissors=new RpsChoice(msg,losses,wins,ties,"Scissors",game);

    	Rock.setOnAction(rock);
    	Paper.setOnAction(paper);
    	Scissors.setOnAction(scissors);

		stage.show(); 
	}
    public static void main(String[] args) {
        Application.launch(args);
    }
}